/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
            'robotoregular': '<link rel=\"stylesheet\" href=\"font/stylesheet.css\" type=\"text/css\" media=\"screen\" title=\"\" charset=\"utf-8\">   ',
            'nofont': '<link rel=\"stylesheet\" href=\"css/style.css\">'        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
            js+"jquery-2.0.3.min.js",
            js+"jquery.easing.1.3.js"
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'mapa_final',
                            type: 'image',
                            rect: ['-45px', '135px', '3108px', '934px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"mapa_final.png",'0px','0px'],
                            userClass: ""
                        },
                        {
                            id: 'nombres_paises',
                            type: 'image',
                            rect: ['95px', '331px', '2701px', '621px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"nombres_paises.png",'0px','0px']
                        },
                        {
                            id: 'mexico',
                            type: 'image',
                            rect: ['2293px', '538px', '49px', '11px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"mexico.png",'0px','0px']
                        },
                        {
                            id: 'franja',
                            type: 'rect',
                            rect: ['0px', '-1px', '1796px', '145px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0.40)"],
                            stroke: [0,"rgba(0,0,0,1)","none"],
                            userClass: "nav"
                        },
                        {
                            id: 'titulo',
                            type: 'text',
                            rect: ['16px', '7px', '287px', '70px', 'auto', 'auto'],
                            text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255);\">Distribución mundial de&nbsp;</span></p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\">​combustibles fósiles</span></p><p style=\"margin: 0px;\"><span style=\"color: rgb(255, 255, 255);\">​</span></p>",
                            userClass: "nav",
                            font: ['robotoregular', [26, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                        },
                        {
                            id: 'botonera',
                            type: 'group',
                            rect: ['9', '87', '194', '100', 'auto', 'auto'],
                            userClass: "nav",
                            c: [
                            {
                                id: 'btn_carbon',
                                type: 'group',
                                rect: ['10px', '0px', '39', '39', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'boton_carbon',
                                    type: 'image',
                                    rect: ['0px', '0px', '39px', '39px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_carbon.png",'0px','0px']
                                },
                                {
                                    id: 'boton_carbon_hover',
                                    type: 'image',
                                    rect: ['1px', '1px', '37px', '37px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_carbon_hover.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'cuadro_carbon_hover',
                                type: 'image',
                                rect: ['0px', '45px', '58px', '54px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cuadro_carbon_hover.png",'0px','0px']
                            },
                            {
                                id: 'btn_petroleo',
                                type: 'group',
                                rect: ['79px', '0px', '39', '39', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'boton_petroleo',
                                    type: 'image',
                                    rect: ['0px', '0px', '39px', '39px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_petroleo.png",'0px','0px']
                                },
                                {
                                    id: 'boton_petroleo_hover',
                                    type: 'image',
                                    rect: ['1px', '0px', '37px', '37px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_petroleo_hover.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'cuadro_petroleo_hover',
                                type: 'image',
                                rect: ['68px', '46px', '58px', '54px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cuadro_petroleo_hover.png",'0px','0px']
                            },
                            {
                                id: 'btn_gas',
                                type: 'group',
                                rect: ['146px', '0px', '39', '39', 'auto', 'auto'],
                                cursor: 'pointer',
                                c: [
                                {
                                    id: 'boton_gas',
                                    type: 'image',
                                    rect: ['0px', '0px', '39px', '39px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_gas.png",'0px','0px']
                                },
                                {
                                    id: 'boton_gas_hover',
                                    type: 'image',
                                    rect: ['1px', '1px', '37px', '37px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"boton_gas_hover.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'cuadro_gas_hover',
                                type: 'image',
                                rect: ['136px', '46px', '58px', '54px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"cuadro_gas_hover.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'minimapa',
                            type: 'group',
                            rect: ['333', '15px', '233', '117', 'auto', 'auto'],
                            userClass: "nav",
                            c: [
                            {
                                id: 'mini_mapa',
                                type: 'image',
                                rect: ['0px', '0px', '233px', '117px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"mini_mapa.png",'0px','0px']
                            },
                            {
                                id: 'sombras',
                                type: 'group',
                                rect: ['2', '2', '229', '114', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'sombra_oeste_norte',
                                    type: 'rect',
                                    rect: ['0px', '0px', '114px', '55px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0.40)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'sombra_oeste_sur',
                                    type: 'rect',
                                    rect: ['0px', '56px', '114px', '58px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0.40)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    transform: [[],[],['-1']]
                                },
                                {
                                    id: 'sombra_este_sur',
                                    type: 'rect',
                                    rect: ['115px', '56px', '114px', '58px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0.40)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'sombra_este_norte',
                                    type: 'rect',
                                    rect: ['115px', '0px', '114px', '55px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0.40)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                }]
                            },
                            {
                                id: 'boton_oeste_sur',
                                type: 'rect',
                                rect: ['0px', '59px', '118px', '56px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'boton_oeste_norte',
                                type: 'rect',
                                rect: ['0px', '0px', '118px', '56px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'boton_este_sur',
                                type: 'rect',
                                rect: ['118px', '59px', '112px', '55px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'boton_este_norte',
                                type: 'rect',
                                rect: ['118px', '1px', '112px', '55px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'mensaje',
                                type: 'image',
                                rect: ['-360px', '96px', '286px', '166px', 'auto', 'auto'],
                                opacity: '0',
                                fill: ["rgba(0,0,0,0)",im+"mensaje.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'simbologia',
                            type: 'image',
                            rect: ['1497px', '28px', '261px', '86px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"simbologia.png",'0px','0px'],
                            userClass: "nav"
                        },
                        {
                            id: 'iconos_carbon',
                            type: 'group',
                            rect: ['243px', '294px', '2534', '555', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_carbon',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['0px', '34px', '25', '31', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon2',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['84px', '8px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon3',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['200px', '39px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon4',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['360px', '25px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon5',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['596px', '0px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon6',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['571px', '235px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon7',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['806px', '182px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon8',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['1014px', '524px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon9',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['130px', '508px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon10',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['2078px', '0px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon11',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['2081px', '115px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon12',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['2291px', '324px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_carbon13',
                                symbolName: 'icon_carbon',
                                type: 'rect',
                                rect: ['2522px', '483px', 'undefined', 'undefined', 'auto', 'auto']
                            }]
                        },
                        {
                            id: 'iconos_petroleo',
                            type: 'group',
                            rect: ['211px', '287px', '2459', '384', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_petroleo',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['82px', '185px', '20', '38', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo2',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['4px', '254px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo3',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['290px', '146px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo4',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['366px', '188px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo5',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['308px', '258px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo6',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['411px', '222px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo7',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['409px', '160px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo8',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['421px', '25px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo9',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['656px', '3px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo10',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['2142px', '0px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo11',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['2147px', '115px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_petroleo12',
                                symbolName: 'icon_petroleo',
                                type: 'rect',
                                rect: ['2439px', '346px', 'undefined', 'undefined', 'auto', 'auto']
                            }]
                        },
                        {
                            id: 'iconos_gas',
                            type: 'group',
                            rect: ['126px', '297px', '2571', '632', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_gas',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['9px', '182px', '24', '31', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas2',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['111px', '251px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas3',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['395px', '143px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas4',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['457px', '214px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas5',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['418px', '255px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas6',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['517px', '218px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas7',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['516px', '155px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas8',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['558px', '83px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas9',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['531px', '21px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas10',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['772px', '0px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas11',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['1160px', '521px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas12',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2261px', '111px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas13',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2547px', '342px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas14',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2440px', '444px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas15',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2485px', '498px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas16',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2494px', '601px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas17',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2178px', '207px', 'undefined', 'undefined', 'auto', 'auto']
                            },
                            {
                                id: 'icon_gas18',
                                symbolName: 'icon_gas',
                                type: 'rect',
                                rect: ['2612px', '480px', 'undefined', 'undefined', 'auto', 'auto']
                            }]
                        },
                        {
                            id: 'pop',
                            type: 'group',
                            rect: ['108px', '-1px', '3140', '1136', 'auto', 'auto'],
                            c: [
                            {
                                id: 'cuadros_popup',
                                symbolName: 'cuadros_popup',
                                type: 'rect',
                                rect: ['-107px', '-12px', '3140', '1136', 'auto', 'auto']
                            }]
                        },
                        {
                            id: 'mano1',
                            type: 'group',
                            rect: ['299', '348', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', '46', '46', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano2',
                            type: 'group',
                            rect: ['490', '346', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano2',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano3',
                            type: 'group',
                            rect: ['169', '498', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano3',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano4',
                            type: 'group',
                            rect: ['268', '568', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano4',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano5',
                            type: 'group',
                            rect: ['322', '500', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano5',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano6',
                            type: 'group',
                            rect: ['478', '508', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano6',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['105px', '74px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano7',
                            type: 'group',
                            rect: ['660', '470', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano7',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano8',
                            type: 'group',
                            rect: ['962', '319', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano8',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano9',
                            type: 'group',
                            rect: ['1087', '497', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano9',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano10',
                            type: 'group',
                            rect: ['1329', '838', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano10',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano11',
                            type: 'group',
                            rect: ['2385', '314', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano11',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano12',
                            type: 'group',
                            rect: ['2461', '432', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano12',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano13',
                            type: 'group',
                            rect: ['2596', '636', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano13',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano14',
                            type: 'group',
                            rect: ['2725', '657', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano14',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano15',
                            type: 'group',
                            rect: ['2791', '799', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano15',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        },
                        {
                            id: 'mano16',
                            type: 'group',
                            rect: ['2565', '861', '46', '46', 'auto', 'auto'],
                            c: [
                            {
                                id: 'icon_mano16',
                                symbolName: 'icon_mano',
                                type: 'rect',
                                rect: ['0px', '0px', 'undefined', 'undefined', 'auto', 'auto'],
                                cursor: 'pointer'
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '3150px', '1118px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,0.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 500,
                    autoPlay: true,
                    data: [
                        [
                            "eid286",
                            "top",
                            0,
                            0,
                            "linear",
                            "${icon_mano}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid323",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${mensaje}",
                            '0',
                            '1'
                        ],
                        [
                            "eid285",
                            "left",
                            0,
                            0,
                            "linear",
                            "${icon_mano}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid287",
                            "left",
                            0,
                            0,
                            "linear",
                            "${cuadros_popup}",
                            '-107px',
                            '-107px'
                        ]
                    ]
                }
            },
            "icon_carbon": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'icono_carbon',
                            opacity: '0',
                            rect: ['0px', '0px', '25px', '31px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/icono_carbon.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '25px', '31px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: false,
                    labels: {
                        "inicio": 0,
                        "ok": 500
                    },
                    data: [
                        [
                            "eid16",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '0',
                            '1'
                        ],
                        [
                            "eid18",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '1',
                            '0'
                        ],
                        [
                            "eid19",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '0',
                            '1'
                        ],
                        [
                            "eid20",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '1',
                            '0'
                        ],
                        [
                            "eid21",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '0',
                            '1'
                        ],
                        [
                            "eid22",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '1',
                            '0'
                        ],
                        [
                            "eid23",
                            "opacity",
                            3000,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '0',
                            '1'
                        ],
                        [
                            "eid24",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${icono_carbon}",
                            '1',
                            '0'
                        ]
                    ]
                }
            },
            "icon_petroleo": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '20px', '38px', 'auto', 'auto'],
                            id: 'icono_petroleo',
                            opacity: '0',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/icono_petroleo.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '20px', '38px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: false,
                    labels: {
                        "inicio": 0,
                        "ok": 500
                    },
                    data: [
                        [
                            "eid35",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '0',
                            '1'
                        ],
                        [
                            "eid37",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '1',
                            '0'
                        ],
                        [
                            "eid39",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '0',
                            '1'
                        ],
                        [
                            "eid38",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '1',
                            '0'
                        ],
                        [
                            "eid41",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '0',
                            '1'
                        ],
                        [
                            "eid40",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '1',
                            '0'
                        ],
                        [
                            "eid43",
                            "opacity",
                            3000,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '0',
                            '1'
                        ],
                        [
                            "eid42",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${icono_petroleo}",
                            '1',
                            '0'
                        ]
                    ]
                }
            },
            "icon_gas": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'icono_gas',
                            opacity: '0',
                            rect: ['0px', '0px', '24px', '31px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/icono_gas.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '24px', '31px']
                        }
                    }
                },
                timeline: {
                    duration: 4000,
                    autoPlay: false,
                    labels: {
                        "inicio": 0,
                        "ok": 500
                    },
                    data: [
                        [
                            "eid49",
                            "opacity",
                            0,
                            500,
                            "linear",
                            "${icono_gas}",
                            '0',
                            '1'
                        ],
                        [
                            "eid51",
                            "opacity",
                            500,
                            500,
                            "linear",
                            "${icono_gas}",
                            '1',
                            '0'
                        ],
                        [
                            "eid53",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${icono_gas}",
                            '0',
                            '1'
                        ],
                        [
                            "eid52",
                            "opacity",
                            1500,
                            500,
                            "linear",
                            "${icono_gas}",
                            '1',
                            '0'
                        ],
                        [
                            "eid55",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${icono_gas}",
                            '0',
                            '1'
                        ],
                        [
                            "eid54",
                            "opacity",
                            2500,
                            500,
                            "linear",
                            "${icono_gas}",
                            '1',
                            '0'
                        ],
                        [
                            "eid57",
                            "opacity",
                            3000,
                            500,
                            "linear",
                            "${icono_gas}",
                            '0',
                            '1'
                        ],
                        [
                            "eid56",
                            "opacity",
                            3500,
                            500,
                            "linear",
                            "${icono_gas}",
                            '1',
                            '0'
                        ]
                    ]
                }
            },
            "icon_mano": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            transform: [[], [], [], ['0.85', '0.85']],
                            rect: ['0px', '0px', '46px', '46px', 'auto', 'auto'],
                            id: 'icon_mano',
                            opacity: '0',
                            display: 'none',
                            fill: ['rgba(0,0,0,0)', 'images/icon_mano.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '46px', '46px']
                        }
                    }
                },
                timeline: {
                    duration: 5830,
                    autoPlay: false,
                    labels: {
                        "inicio": 0,
                        "press": 1330
                    },
                    data: [
                        [
                            "eid68",
                            "scaleY",
                            1330,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid71",
                            "scaleY",
                            2080,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid85",
                            "scaleY",
                            2830,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid86",
                            "scaleY",
                            3580,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid89",
                            "scaleY",
                            4330,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid90",
                            "scaleY",
                            5080,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid67",
                            "scaleX",
                            1330,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid72",
                            "scaleX",
                            2080,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid87",
                            "scaleX",
                            2830,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid88",
                            "scaleX",
                            3580,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid91",
                            "scaleX",
                            4330,
                            750,
                            "linear",
                            "${icon_mano}",
                            '1',
                            '0.9'
                        ],
                        [
                            "eid92",
                            "scaleX",
                            5080,
                            750,
                            "linear",
                            "${icon_mano}",
                            '0.9',
                            '1'
                        ],
                        [
                            "eid62",
                            "opacity",
                            250,
                            1080,
                            "linear",
                            "${icon_mano}",
                            '0',
                            '1'
                        ],
                        [
                            "eid94",
                            "display",
                            0,
                            0,
                            "linear",
                            "${icon_mano}",
                            'none',
                            'none'
                        ],
                        [
                            "eid95",
                            "display",
                            250,
                            0,
                            "linear",
                            "${icon_mano}",
                            'none',
                            'block'
                        ]
                    ]
                }
            },
            "cuadros_popup": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '3140px', '1136px', 'auto', 'auto'],
                            id: 'parhce_loco',
                            stroke: [0, 'rgb(0, 0, 0)', 'none'],
                            type: 'rect',
                            fill: ['rgba(192,192,192,0)']
                        },
                        {
                            rect: ['104px', '215px', '305px', '174px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup1',
                            opacity: '0',
                            clip: 'rect(165px 305px 174px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup1.png', '0px', '0px']
                        },
                        {
                            rect: ['302px', '201px', '305px', '189px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup2',
                            opacity: '0',
                            clip: 'rect(174px 305px 189px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup22.png', '0px', '0px']
                        },
                        {
                            rect: ['-5px', '355px', '305px', '175px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup3',
                            opacity: '0',
                            clip: 'rect(164px 305px 175px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup32.png', '0px', '0px']
                        },
                        {
                            rect: ['85px', '374px', '306px', '221px', 'auto', 'auto'],
                            type: 'image',
                            display: 'block',
                            id: 'popup4',
                            opacity: '0',
                            clip: 'rect(211px 306px 221px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup4.png', '0px', '0px']
                        },
                        {
                            rect: ['79px', '595px', '306px', '218px', 'auto', 'auto'],
                            type: 'image',
                            display: 'block',
                            id: 'popup_nigeria',
                            opacity: '0',
                            clip: 'rect(0px 306px 25px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup_nigeria.png', '0px', '0px']
                        },
                        {
                            rect: ['151px', '332px', '305px', '189px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup5',
                            opacity: '0',
                            clip: 'rect(177px 305px 189px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup53.png', '0px', '0px']
                        },
                        {
                            rect: ['395px', '599px', '305px', '188px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup6',
                            opacity: '0',
                            clip: 'rect(0px 305px 14px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup6.png', '0px', '0px']
                        },
                        {
                            rect: ['499px', '329px', '305px', '170px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup7',
                            opacity: '0',
                            clip: 'rect(157px 305px 170px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup7.png', '0px', '0px']
                        },
                        {
                            rect: ['757px', '169px', '306px', '176px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup8',
                            opacity: '0',
                            clip: 'rect(161px 306px 176px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup8.png', '0px', '0px']
                        },
                        {
                            rect: ['909px', '332px', '305px', '205px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup9',
                            opacity: '0',
                            clip: 'rect(199px 305px 205px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup9.png', '0px', '0px']
                        },
                        {
                            rect: ['1144px', '664px', '305px', '204px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup10',
                            opacity: '0',
                            clip: 'rect(192px 305px 204px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup10.png', '0px', '0px']
                        },
                        {
                            rect: ['2195px', '134px', '305px', '204px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup11',
                            opacity: '0',
                            clip: 'rect(189.33349609375px 305px 204px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup11.png', '0px', '0px']
                        },
                        {
                            rect: ['2230px', '451px', '305px', '234px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup12',
                            opacity: '0',
                            clip: 'rect(0px 305px 24.666748046875px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup12.png', '0px', '0px']
                        },
                        {
                            rect: ['2394px', '485px', '306px', '189px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup13',
                            opacity: '0',
                            clip: 'rect(178.66650390625px 306px 189px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup13.png', '0px', '0px']
                        },
                        {
                            rect: ['2506px', '492px', '305px', '189px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup14',
                            opacity: '0',
                            clip: 'rect(178.666748046875px 305px 189px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup14.png', '0px', '0px']
                        },
                        {
                            rect: ['2610px', '810px', '306px', '186px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup15',
                            opacity: '0',
                            clip: 'rect(0px 306px 16.66650390625px 0px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup15.png', '0px', '0px']
                        },
                        {
                            rect: ['2286px', '765px', '322px', '199px', 'auto', 'auto'],
                            type: 'image',
                            display: 'none',
                            id: 'popup16',
                            opacity: '0',
                            clip: 'rect(0px 322px 199px 314px)',
                            fill: ['rgba(0,0,0,0)', 'images/popup16.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '3140px', '1136px']
                        }
                    }
                },
                timeline: {
                    duration: 16710,
                    autoPlay: false,
                    labels: {
                        "inicio": 0,
                        "pop1": 1000,
                        "pop2": 2000,
                        "pop3": 3000,
                        "pop4": 4000,
                        "pop5": 5000,
                        "pop6": 6000,
                        "pop7": 7000,
                        "pop8": 8000,
                        "pop9": 9000,
                        "pop10": 10000,
                        "pop11": 11000,
                        "pop12": 12000,
                        "pop13": 13000,
                        "pop14": 14000,
                        "pop15": 15000,
                        "pop16": 16000
                    },
                    data: [
                        [
                            "eid228",
                            "display",
                            11000,
                            0,
                            "linear",
                            "${popup11}",
                            'none',
                            'block'
                        ],
                        [
                            "eid227",
                            "display",
                            11500,
                            0,
                            "linear",
                            "${popup11}",
                            'block',
                            'block'
                        ],
                        [
                            "eid229",
                            "display",
                            11750,
                            0,
                            "linear",
                            "${popup11}",
                            'block',
                            'none'
                        ],
                        [
                            "eid126",
                            "display",
                            0,
                            0,
                            "linear",
                            "${popup3}",
                            'none',
                            'none'
                        ],
                        [
                            "eid125",
                            "display",
                            3000,
                            0,
                            "linear",
                            "${popup3}",
                            'none',
                            'block'
                        ],
                        [
                            "eid123",
                            "display",
                            3500,
                            0,
                            "linear",
                            "${popup3}",
                            'block',
                            'block'
                        ],
                        [
                            "eid124",
                            "display",
                            3750,
                            0,
                            "linear",
                            "${popup3}",
                            'block',
                            'none'
                        ],
                        [
                            "eid313",
                            "opacity",
                            16000,
                            500,
                            "linear",
                            "${popup16}",
                            '0',
                            '1'
                        ],
                        [
                            "eid293",
                            "display",
                            0,
                            0,
                            "linear",
                            "${popup4}",
                            'block',
                            'none'
                        ],
                        [
                            "eid294",
                            "display",
                            4750,
                            0,
                            "linear",
                            "${popup4}",
                            'none',
                            'none'
                        ],
                        [
                            "eid101",
                            "opacity",
                            1000,
                            500,
                            "linear",
                            "${popup1}",
                            '0',
                            '1'
                        ],
                        [
                            "eid98",
                            "display",
                            1000,
                            0,
                            "linear",
                            "${popup1}",
                            'none',
                            'block'
                        ],
                        [
                            "eid96",
                            "display",
                            1500,
                            0,
                            "linear",
                            "${popup1}",
                            'block',
                            'block'
                        ],
                        [
                            "eid97",
                            "display",
                            1750,
                            0,
                            "linear",
                            "${popup1}",
                            'block',
                            'none'
                        ],
                        [
                            "eid113",
                            "clip",
                            2000,
                            500,
                            "linear",
                            "${popup2}",
                            [174,305,189,0],
                            [2,305,189,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid305",
                            "opacity",
                            8000,
                            500,
                            "linear",
                            "${popup8}",
                            '0',
                            '1'
                        ],
                        [
                            "eid129",
                            "opacity",
                            3000,
                            500,
                            "linear",
                            "${popup3}",
                            '0',
                            '1'
                        ],
                        [
                            "eid248",
                            "opacity",
                            13000,
                            500,
                            "linear",
                            "${popup13}",
                            '0',
                            '1'
                        ],
                        [
                            "eid175",
                            "display",
                            5000,
                            0,
                            "linear",
                            "${popup5}",
                            'none',
                            'block'
                        ],
                        [
                            "eid173",
                            "display",
                            5500,
                            0,
                            "linear",
                            "${popup5}",
                            'block',
                            'block'
                        ],
                        [
                            "eid174",
                            "display",
                            5750,
                            0,
                            "linear",
                            "${popup5}",
                            'block',
                            'none'
                        ],
                        [
                            "eid234",
                            "clip",
                            11000,
                            500,
                            "linear",
                            "${popup11}",
                            [189.33349609375,305,204,0],
                            [0.000244140625,305,204,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid257",
                            "opacity",
                            14000,
                            500,
                            "linear",
                            "${popup14}",
                            '0',
                            '1'
                        ],
                        [
                            "eid301",
                            "display",
                            8000,
                            0,
                            "linear",
                            "${popup8}",
                            'none',
                            'block'
                        ],
                        [
                            "eid302",
                            "display",
                            8750,
                            0,
                            "linear",
                            "${popup8}",
                            'block',
                            'none'
                        ],
                        [
                            "eid266",
                            "clip",
                            15000,
                            500,
                            "linear",
                            "${popup15}",
                            [0,306,16.66650390625,0],
                            [0,306,183.3330078125,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid186",
                            "display",
                            6000,
                            0,
                            "linear",
                            "${popup6}",
                            'none',
                            'block'
                        ],
                        [
                            "eid182",
                            "display",
                            6500,
                            0,
                            "linear",
                            "${popup6}",
                            'block',
                            'block'
                        ],
                        [
                            "eid185",
                            "display",
                            6750,
                            0,
                            "linear",
                            "${popup6}",
                            'block',
                            'none'
                        ],
                        [
                            "eid332",
                            "clip",
                            4000,
                            500,
                            "linear",
                            "${popup_nigeria}",
                            [0,306,25,0],
                            [0,306,212,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid333",
                            "opacity",
                            4000,
                            500,
                            "linear",
                            "${popup_nigeria}",
                            '0',
                            '1'
                        ],
                        [
                            "eid177",
                            "opacity",
                            5000,
                            500,
                            "linear",
                            "${popup5}",
                            '0',
                            '1'
                        ],
                        [
                            "eid252",
                            "clip",
                            13000,
                            500,
                            "linear",
                            "${popup13}",
                            [178.66650390625,306,189,0],
                            [2.666748046875,306,189,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid309",
                            "display",
                            16000,
                            0,
                            "linear",
                            "${popup16}",
                            'none',
                            'block'
                        ],
                        [
                            "eid310",
                            "display",
                            16710,
                            0,
                            "linear",
                            "${popup16}",
                            'block',
                            'none'
                        ],
                        [
                            "eid308",
                            "clip",
                            8000,
                            500,
                            "linear",
                            "${popup8}",
                            [161,306,176,0],
                            [1,306,176,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid181",
                            "clip",
                            5000,
                            500,
                            "linear",
                            "${popup5}",
                            [177,305,189,0],
                            [-1,305,189,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid224",
                            "opacity",
                            10000,
                            500,
                            "linear",
                            "${popup10}",
                            '0',
                            '1'
                        ],
                        [
                            "eid104",
                            "clip",
                            1000,
                            500,
                            "linear",
                            "${popup1}",
                            [165,305,174,0],
                            [-2,305,174,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid300",
                            "clip",
                            4000,
                            500,
                            "linear",
                            "${popup4}",
                            [211,306,221,0],
                            [2,306,221,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid236",
                            "display",
                            12000,
                            0,
                            "linear",
                            "${popup12}",
                            'none',
                            'block'
                        ],
                        [
                            "eid235",
                            "display",
                            12500,
                            0,
                            "linear",
                            "${popup12}",
                            'block',
                            'block'
                        ],
                        [
                            "eid237",
                            "display",
                            12750,
                            0,
                            "linear",
                            "${popup12}",
                            'block',
                            'none'
                        ],
                        [
                            "eid260",
                            "display",
                            15000,
                            0,
                            "linear",
                            "${popup15}",
                            'none',
                            'block'
                        ],
                        [
                            "eid261",
                            "display",
                            15500,
                            0,
                            "linear",
                            "${popup15}",
                            'block',
                            'block'
                        ],
                        [
                            "eid262",
                            "display",
                            15750,
                            0,
                            "linear",
                            "${popup15}",
                            'block',
                            'none'
                        ],
                        [
                            "eid198",
                            "opacity",
                            7000,
                            500,
                            "linear",
                            "${popup7}",
                            '0',
                            '1'
                        ],
                        [
                            "eid244",
                            "display",
                            13000,
                            0,
                            "linear",
                            "${popup13}",
                            'none',
                            'block'
                        ],
                        [
                            "eid243",
                            "display",
                            13500,
                            0,
                            "linear",
                            "${popup13}",
                            'block',
                            'block'
                        ],
                        [
                            "eid245",
                            "display",
                            13750,
                            0,
                            "linear",
                            "${popup13}",
                            'block',
                            'none'
                        ],
                        [
                            "eid106",
                            "display",
                            2000,
                            0,
                            "linear",
                            "${popup2}",
                            'none',
                            'block'
                        ],
                        [
                            "eid105",
                            "display",
                            2500,
                            0,
                            "linear",
                            "${popup2}",
                            'block',
                            'block'
                        ],
                        [
                            "eid107",
                            "display",
                            2750,
                            0,
                            "linear",
                            "${popup2}",
                            'block',
                            'none'
                        ],
                        [
                            "eid325",
                            "display",
                            4000,
                            0,
                            "linear",
                            "${popup_nigeria}",
                            'block',
                            'block'
                        ],
                        [
                            "eid324",
                            "display",
                            4500,
                            0,
                            "linear",
                            "${popup_nigeria}",
                            'block',
                            'block'
                        ],
                        [
                            "eid326",
                            "display",
                            4750,
                            0,
                            "linear",
                            "${popup_nigeria}",
                            'block',
                            'none'
                        ],
                        [
                            "eid216",
                            "opacity",
                            9000,
                            500,
                            "linear",
                            "${popup9}",
                            '0',
                            '1'
                        ],
                        [
                            "eid132",
                            "clip",
                            3000,
                            500,
                            "linear",
                            "${popup3}",
                            [164,305,175,0],
                            [-2,305,175,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid259",
                            "clip",
                            14000,
                            500,
                            "linear",
                            "${popup14}",
                            [178.666748046875,305,189,0],
                            [-2.66650390625,305,189,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid264",
                            "opacity",
                            15000,
                            500,
                            "linear",
                            "${popup15}",
                            '0',
                            '1'
                        ],
                        [
                            "eid201",
                            "clip",
                            7000,
                            500,
                            "linear",
                            "${popup7}",
                            [157,305,170,0],
                            [0,305,170,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid192",
                            "clip",
                            6000,
                            500,
                            "linear",
                            "${popup6}",
                            [0,305,14,0],
                            [0,305,193,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid189",
                            "opacity",
                            6000,
                            500,
                            "linear",
                            "${popup6}",
                            '0',
                            '1'
                        ],
                        [
                            "eid211",
                            "display",
                            9000,
                            0,
                            "linear",
                            "${popup9}",
                            'none',
                            'block'
                        ],
                        [
                            "eid212",
                            "display",
                            9500,
                            0,
                            "linear",
                            "${popup9}",
                            'block',
                            'block'
                        ],
                        [
                            "eid213",
                            "display",
                            9750,
                            0,
                            "linear",
                            "${popup9}",
                            'block',
                            'none'
                        ],
                        [
                            "eid221",
                            "display",
                            10000,
                            0,
                            "linear",
                            "${popup10}",
                            'none',
                            'block'
                        ],
                        [
                            "eid220",
                            "display",
                            10500,
                            0,
                            "linear",
                            "${popup10}",
                            'block',
                            'block'
                        ],
                        [
                            "eid222",
                            "display",
                            10791,
                            0,
                            "linear",
                            "${popup10}",
                            'block',
                            'none'
                        ],
                        [
                            "eid110",
                            "opacity",
                            2000,
                            500,
                            "linear",
                            "${popup2}",
                            '0',
                            '1'
                        ],
                        [
                            "eid297",
                            "opacity",
                            4000,
                            500,
                            "linear",
                            "${popup4}",
                            '0',
                            '1'
                        ],
                        [
                            "eid231",
                            "opacity",
                            11000,
                            500,
                            "linear",
                            "${popup11}",
                            '0',
                            '1'
                        ],
                        [
                            "eid254",
                            "display",
                            14000,
                            0,
                            "linear",
                            "${popup14}",
                            'none',
                            'block'
                        ],
                        [
                            "eid253",
                            "display",
                            14500,
                            0,
                            "linear",
                            "${popup14}",
                            'block',
                            'block'
                        ],
                        [
                            "eid255",
                            "display",
                            14750,
                            0,
                            "linear",
                            "${popup14}",
                            'block',
                            'none'
                        ],
                        [
                            "eid226",
                            "clip",
                            10000,
                            500,
                            "linear",
                            "${popup10}",
                            [192,305,204,0],
                            [-1,305,204,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid242",
                            "clip",
                            12000,
                            500,
                            "linear",
                            "${popup12}",
                            [0,305,24.666748046875,0],
                            [0,305,236.666748046875,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid239",
                            "opacity",
                            12000,
                            500,
                            "linear",
                            "${popup12}",
                            '0',
                            '1'
                        ],
                        [
                            "eid317",
                            "clip",
                            16000,
                            500,
                            "linear",
                            "${popup16}",
                            [0,322,199,314],
                            [0,322,199,2],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid219",
                            "clip",
                            9000,
                            500,
                            "linear",
                            "${popup9}",
                            [199,305,205,0],
                            [0,305,205,0],
                            {valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'}
                        ],
                        [
                            "eid194",
                            "display",
                            7000,
                            0,
                            "linear",
                            "${popup7}",
                            'none',
                            'block'
                        ],
                        [
                            "eid193",
                            "display",
                            7500,
                            0,
                            "linear",
                            "${popup7}",
                            'block',
                            'block'
                        ],
                        [
                            "eid195",
                            "display",
                            7750,
                            0,
                            "linear",
                            "${popup7}",
                            'block',
                            'none'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("distribucion_mundial_edgeActions.js");
})("EDGE-7761605");
