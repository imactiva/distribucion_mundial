/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindSymbolAction(compId, symbolName, "creationComplete", function(sym, e) {
         //sym.getSymbol("cuadros_popup").$("parhce_loco").hide();
         sym.buenas = 0;
         ok1 = 0;
         ok2 = 0;
         ok3 = 0;
         ok4 = 0;
         
         ////fondo imagen
         $('body').css("background-image", "url(images/fondo_mapa.jpg)");  
         $('body').css("background-size", "cover");  
         
         ////////navegacion
         sym.$("cuadro_petroleo_hover").hide(); 
         sym.$("cuadro_carbon_hover").hide();
         sym.$("cuadro_gas_hover").hide();
         sym.$("boton_petroleo_hover").hide();
         sym.$("boton_carbon_hover").hide()
         sym.$("boton_gas_hover").hide()
         
         ///codigo loco
         //sym.$("body").append(sym.$("botonera").css({"position":"fixed"}));
         //sym.$("body").append(sym.$("titulo").css({"position":"fixed"}));
         //sym.$("body").append(sym.$("minimapa").css({"position":"fixed"}));
         //sym.$("body").append(sym.$("franja").css({"position":"fixed"}));
         //sym.$("body").append(sym.$("simbologia").css({"position":"fixed"}));
         
         
         $(window).scroll(function(evt){
         	var scrolly = (evt.currentTarget.scrollY)? evt.currentTarget.scrollY:document.documentElement.scrollTop;
         	var scrollx = (evt.currentTarget.scrollX)? evt.currentTarget.scrollX:document.documentElement.scrollLeft;
         
         	sym.$("titulo").css("top", 10 + scrolly);
         	sym.$("titulo").css("left", 10 + scrollx);
         	sym.$("botonera").css("top", 95 + scrolly);
         	sym.$("botonera").css("left", 5 + scrollx);
         	sym.$("minimapa").css("top", 10 + scrolly);
         	sym.$("minimapa").css("left", 320 + scrollx);
         	sym.$("franja").css("top", 0 + scrolly);
         	sym.$("franja").css("left", 0 + scrollx);
         	sym.$("simbologia").css("top", 10 + scrolly);
         
         
         	if($(window).width() < 868){
         		sym.$("simbologia").css("top",$(window).height()+ scrolly - 100);
         		sym.$("simbologia").css("left", 10 + scrollx);
         	}else{
         		sym.$("simbologia").css("left", 10);
         		sym.$("simbologia").css("left", $(window).width()+ scrollx - 281);
         	}
         
         	console.log(evt);
         
         });
         
         $(window).scroll();
         $(window).resize(function(){
         $(window).scroll();
         
         });
         
         //////////////
         /*
         var top1 = sym.$("botonera").offset().top;
         
         $(window).scroll(function(evt){
         	console.log(evt.currentTarget.scrollY);
         
         
         	sym.$("botonera").css("top", top1 + evt.currentTarget.scrollY);
         	console.log(top1);
         
         });
         
         
         sym.$("botonera").css({position: "fixed", top:95});
         sym.$("botonera").css({position: "fixed", left:5});
         sym.$("titulo").css({position: "fixed", left:15});
         sym.$("minimapa").css({position: "fixed", left:320});
         sym.$("franja").css({position: "fixed", left:0});
         sym.$("franja").css({position: "fixed", top:0});
         //$('#Stage_simbologia').css({left: '', right: 10, top: 15, position: 'fixed'});
         */
         sym.$("titulo").css('zIndex',450);
         sym.$("minimapa").css('zIndex',450);
         sym.$("franja").css('zIndex',400);
         sym.$("botonera").css('zIndex',455);
         sym.$("sombra_oeste_norte").hide();
         
         
         sym.$("franja").hide();
         sym.$("simbologia").css('zIndex',1050);
         
         
         
         
         $(window).scroll(function(evt){
            var position = $(window).scrollTop();
         	if(position > 268 && position < 450){
         			//sym.$("franja").show();					
         			sym.$("franja").fadeIn( "slow" );
         
         		}else if(position > 0 && position < 267){
         		  //sym.$("franja").hide();					
         			sym.$("franja").fadeOut( "slow" );
         		}
         
         });
         
         /*
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("inicio");
         sym.getSymbol("icon_carbon2").stop("inicio");
         sym.getSymbol("icon_carbon3").stop("inicio");
         sym.getSymbol("icon_carbon4").stop("inicio");
         sym.getSymbol("icon_carbon5").stop("inicio");
         sym.getSymbol("icon_carbon6").stop("inicio");
         sym.getSymbol("icon_carbon7").stop("inicio");
         sym.getSymbol("icon_carbon8").stop("inicio");
         sym.getSymbol("icon_carbon9").stop("inicio");
         sym.getSymbol("icon_carbon10").stop("inicio");
         sym.getSymbol("icon_carbon11").stop("inicio");
         sym.getSymbol("icon_carbon12").stop("inicio");
         sym.getSymbol("icon_carbon13").stop("inicio");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("inicio");
         sym.getSymbol("icon_petroleo2").stop("inicio");
         sym.getSymbol("icon_petroleo3").stop("inicio");
         sym.getSymbol("icon_petroleo4").stop("inicio");
         sym.getSymbol("icon_petroleo5").stop("inicio");
         sym.getSymbol("icon_petroleo6").stop("inicio");
         sym.getSymbol("icon_petroleo7").stop("inicio");
         sym.getSymbol("icon_petroleo8").stop("inicio");
         sym.getSymbol("icon_petroleo9").stop("inicio");
         sym.getSymbol("icon_petroleo10").stop("inicio");
         sym.getSymbol("icon_petroleo11").stop("inicio");
         sym.getSymbol("icon_petroleo12").stop("inicio");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("inicio");
         sym.getSymbol("icon_gas2").stop("inicio");
         sym.getSymbol("icon_gas3").stop("inicio");
         sym.getSymbol("icon_gas4").stop("inicio");
         sym.getSymbol("icon_gas5").stop("inicio");
         sym.getSymbol("icon_gas6").stop("inicio");
         sym.getSymbol("icon_gas7").stop("inicio");
         sym.getSymbol("icon_gas8").stop("inicio");
         sym.getSymbol("icon_gas9").stop("inicio");
         sym.getSymbol("icon_gas10").stop("inicio");
         sym.getSymbol("icon_gas11").stop("inicio");
         sym.getSymbol("icon_gas12").stop("inicio");
         sym.getSymbol("icon_gas13").stop("inicio");
         sym.getSymbol("icon_gas14").stop("inicio");
         sym.getSymbol("icon_gas15").stop("inicio");
         sym.getSymbol("icon_gas16").stop("inicio");
         sym.getSymbol("icon_gas17").stop("inicio");
         sym.getSymbol("icon_gas18").stop("inicio");
         */
         
         
         ///variables manos
         sym.manito1 = function(){
            if(ok1 == 1 & ok2 == 1 ){
         		sym.$("icon_mano4").hide();
         
         	} 
         }	
         
         
         sym.manito2 = function(){
            if(ok3 == 1 & ok4 == 1 ){
         		sym.$("icon_mano11").hide();
         
         	} 
         }	
         
         
         
         //
         sym.$("pop").css('zIndex',15);
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: GAS
         sym.$("icon_gas").hide();
         sym.$("icon_gas2").hide();
         sym.$("icon_gas3").hide();
         sym.$("icon_gas4").hide();
         sym.$("icon_gas5").hide();
         sym.$("icon_gas6").hide();
         sym.$("icon_gas7").hide();
         sym.$("icon_gas8").hide();
         sym.$("icon_gas9").hide();
         sym.$("icon_gas10").hide();
         sym.$("icon_gas11").hide();
         sym.$("icon_gas12").hide();
         sym.$("icon_gas13").hide();
         sym.$("icon_gas14").hide();
         sym.$("icon_gas15").hide();
         sym.$("icon_gas16").hide();
         sym.$("icon_gas17").hide();
         sym.$("icon_gas18").hide();
         
         
         /////////ICONOS: PETROLEO
         sym.$("icon_petroleo").hide();
         sym.$("icon_petroleo2").hide();
         sym.$("icon_petroleo3").hide();
         sym.$("icon_petroleo4").hide();
         sym.$("icon_petroleo5").hide();
         sym.$("icon_petroleo6").hide();
         sym.$("icon_petroleo7").hide();
         sym.$("icon_petroleo8").hide();
         sym.$("icon_petroleo9").hide();
         sym.$("icon_petroleo10").hide();
         sym.$("icon_petroleo11").hide();
         sym.$("icon_petroleo12").hide();
         sym.$("icon_petroleo13").hide();
         
         
         //ICONOS CARBON
         sym.$("icon_carbon").hide();
         sym.$("icon_carbon2").hide();
         sym.$("icon_carbon3").hide();
         sym.$("icon_carbon4").hide();
         sym.$("icon_carbon5").hide();
         sym.$("icon_carbon6").hide();
         sym.$("icon_carbon7").hide();
         sym.$("icon_carbon8").hide();
         sym.$("icon_carbon9").hide();
         sym.$("icon_carbon10").hide();
         sym.$("icon_carbon11").hide();
         sym.$("icon_carbon12").hide();
         sym.$("icon_carbon13").hide();
         
         
         ///manitos
         sym.$("icon_mano").hide();
         sym.$("icon_mano2").hide();
         sym.$("icon_mano3").hide();
         sym.$("icon_mano4").hide();
         sym.$("icon_mano5").hide();
         sym.$("icon_mano6").hide();
         sym.$("icon_mano7").hide();
         sym.$("icon_mano8").hide();
         sym.$("icon_mano9").hide();
         sym.$("icon_mano10").hide();
         sym.$("icon_mano11").hide();
         sym.$("icon_mano12").hide();
         sym.$("icon_mano13").hide();
         sym.$("icon_mano14").hide();
         sym.$("icon_mano15").hide();
         sym.$("icon_mano16").hide();
         
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_gas}", "mouseover", function(sym, e) {
         // introducir código que se ejecute cuando se sitúe el ratón sobre el objeto
         
         if(sym.buenas == 1){
         sym.$("cuadro_gas_hover").show();
         
         }
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_gas}", "mouseleave", function(sym, e) {
         sym.$("cuadro_gas_hover").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_petroleo}", "mouseover", function(sym, e) {
         if(sym.buenas == 1){
         sym.$("cuadro_petroleo_hover").show();
         
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_petroleo}", "mouseleave", function(sym, e) {
         sym.$("cuadro_petroleo_hover").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_carbon}", "mouseover", function(sym, e) {
         if(sym.buenas == 1){
         sym.$("cuadro_carbon_hover").show();
         }

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${btn_carbon}", "mouseleave", function(sym, e) {
         sym.$("cuadro_carbon_hover").hide();
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_oeste_norte}", "click", function(sym, e) {
         sym.$("sombra_oeste_norte").hide();
         sym.$("sombra_oeste_sur").show();
         sym.$("sombra_este_norte").show();
         sym.$("sombra_este_sur").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         
         
           $('html, body').stop().animate({
                             scrollTop: 50,
                             scrollLeft: 0
                         }, 2000);
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_este_norte}", "click", function(sym, e) {
         sym.$("sombra_oeste_norte").show();
         sym.$("sombra_oeste_sur").show();
         sym.$("sombra_este_norte").hide();
         sym.$("sombra_este_sur").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         
           $('html, body').stop().animate({
                             scrollTop: 0,
                             scrollLeft: $("#Stage").width()-$(window).width()
                         }, 2000);

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_oeste_sur}", "click", function(sym, e) {
         
         sym.$("sombra_oeste_norte").show();
         sym.$("sombra_oeste_sur").hide();
         sym.$("sombra_este_norte").show();
         sym.$("sombra_este_sur").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         
           $('html, body').stop().animate({
                             scrollTop: $("#Stage").height()-$(window).height()-220,
                             scrollLeft: 0
                         }, 2000);
         
         
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_este_sur}", "click", function(sym, e) {
         sym.$("sombra_oeste_norte").show();
         sym.$("sombra_oeste_sur").show();
         sym.$("sombra_este_norte").show();
         sym.$("sombra_este_sur").hide();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         
           $('html, body').stop().animate({
                             scrollTop: $("#Stage").height()-$(window).height()-160,
                             scrollLeft: $("#Stage").width()-$(window).width()
                         }, 2000);
         
         /*
           $('html, body').animate({
                             scrollTop: 2000
                         }, 2000);
         
           $('html, body').animate({
                             scrollLeft: 2000
                         }, 2000);  
         */

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${boton_carbon}", "click", function(sym, e) {
         sym.$("boton_carbon").hide();
         sym.$("boton_carbon_hover").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         sym.$("icon_carbon").show();
         sym.$("icon_carbon2").show();
         sym.$("icon_carbon3").show();
         sym.$("icon_carbon4").show();
         sym.$("icon_carbon5").show();
         sym.$("icon_carbon6").show();
         sym.$("icon_carbon7").show();
         sym.$("icon_carbon8").show();
         sym.$("icon_carbon9").show();
         sym.$("icon_carbon10").show();
         sym.$("icon_carbon11").show();
         sym.$("icon_carbon12").show();
         sym.$("icon_carbon13").show();
         /////////ICONOS: CARBON
         /*
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon2").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon4").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon6").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon8").stop("ok");
         sym.getSymbol("icon_carbon9").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         */
         
         
         ///mostrar manitos
         sym.$("icon_mano").show();
         sym.$("icon_mano2").show();
         sym.$("icon_mano9").show();
         sym.$("icon_mano11").show();
         sym.$("icon_mano13").show();
         sym.$("icon_mano15").show();
         
         //
         ok3 = 0;
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon2").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon4").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon6").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon8").stop("ok");
         sym.getSymbol("icon_carbon9").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_carbon_hover}", "click", function(sym, e) {
         sym.$("boton_carbon").show();
         sym.$("boton_carbon_hover").hide();
         //sym.$("mensaje").hide();
         //sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         /*
         sym.getSymbol("icon_carbon").stop("inicio");
         sym.getSymbol("icon_carbon2").stop("inicio");
         sym.getSymbol("icon_carbon3").stop("inicio");
         sym.getSymbol("icon_carbon4").stop("inicio");
         sym.getSymbol("icon_carbon5").stop("inicio");
         sym.getSymbol("icon_carbon6").stop("inicio");
         sym.getSymbol("icon_carbon7").stop("inicio");
         sym.getSymbol("icon_carbon8").stop("inicio");
         sym.getSymbol("icon_carbon9").stop("inicio");
         sym.getSymbol("icon_carbon10").stop("inicio");
         sym.getSymbol("icon_carbon11").stop("inicio");
         sym.getSymbol("icon_carbon12").stop("inicio");
         sym.getSymbol("icon_carbon13").stop("inicio");
         */
         
         sym.$("icon_carbon").hide();
         sym.$("icon_carbon2").hide();
         sym.$("icon_carbon3").hide();
         sym.$("icon_carbon4").hide();
         sym.$("icon_carbon5").hide();
         sym.$("icon_carbon6").hide();
         sym.$("icon_carbon7").hide();
         sym.$("icon_carbon8").hide();
         sym.$("icon_carbon9").hide();
         sym.$("icon_carbon10").hide();
         sym.$("icon_carbon11").hide();
         sym.$("icon_carbon12").hide();
         sym.$("icon_carbon13").hide();
         
         //manitos
         sym.$("icon_mano").hide();
         sym.$("icon_mano2").hide();
         sym.$("icon_mano9").hide();
         sym.$("icon_mano13").hide();
         sym.$("icon_mano15").hide();
         
         ///variables locas
         ok3 = 1;
         sym.manito2();
         
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_petroleo}", "click", function(sym, e) {
         sym.$("boton_petroleo").hide();
         sym.$("boton_petroleo_hover").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: PETROLEO
         sym.$("icon_petroleo").show();
         sym.$("icon_petroleo2").show();
         sym.$("icon_petroleo3").show();
         sym.$("icon_petroleo4").show();
         sym.$("icon_petroleo5").show();
         sym.$("icon_petroleo6").show();
         sym.$("icon_petroleo7").show();
         sym.$("icon_petroleo8").show();
         sym.$("icon_petroleo9").show();
         sym.$("icon_petroleo10").show();
         sym.$("icon_petroleo11").show();
         sym.$("icon_petroleo12").show();
         sym.$("icon_petroleo13").show();
         /*
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo8").stop("ok");
         sym.getSymbol("icon_petroleo9").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         */
         
         ///manitos
         sym.$("icon_mano5").show();
         sym.$("icon_mano6").show();
         sym.$("icon_mano11").show();
         sym.$("icon_mano14").show();
         
         ok2 = 0;
         ok4 = 0;
         sym.$("icon_mano4").show();
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo8").stop("ok");
         sym.getSymbol("icon_petroleo9").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_petroleo_hover}", "click", function(sym, e) {
         sym.$("boton_petroleo").show();
         sym.$("boton_petroleo_hover").hide();
         //sym.$("mensaje").hide();
         //sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         
         /////////ICONOS: PETROLEO
         sym.$("icon_petroleo").hide();
         sym.$("icon_petroleo2").hide();
         sym.$("icon_petroleo3").hide();
         sym.$("icon_petroleo4").hide();
         sym.$("icon_petroleo5").hide();
         sym.$("icon_petroleo6").hide();
         sym.$("icon_petroleo7").hide();
         sym.$("icon_petroleo8").hide();
         sym.$("icon_petroleo9").hide();
         sym.$("icon_petroleo10").hide();
         sym.$("icon_petroleo11").hide();
         sym.$("icon_petroleo12").hide();
         sym.$("icon_petroleo13").hide();
         /*
         sym.getSymbol("icon_petroleo").stop("inicio");
         sym.getSymbol("icon_petroleo2").stop("inicio");
         sym.getSymbol("icon_petroleo3").stop("inicio");
         sym.getSymbol("icon_petroleo4").stop("inicio");
         sym.getSymbol("icon_petroleo5").stop("inicio");
         sym.getSymbol("icon_petroleo6").stop("inicio");
         sym.getSymbol("icon_petroleo7").stop("inicio");
         sym.getSymbol("icon_petroleo8").stop("inicio");
         sym.getSymbol("icon_petroleo9").stop("inicio");
         sym.getSymbol("icon_petroleo10").stop("inicio");
         sym.getSymbol("icon_petroleo11").stop("inicio");
         sym.getSymbol("icon_petroleo12").stop("inicio");
         */
         ///manitos
         sym.$("icon_mano5").hide();
         sym.$("icon_mano6").hide();
         sym.$("icon_mano14").hide();
         ///variables locas
         
         ok2 = 1;
         sym.manito1();
         ok4 = 1;
         sym.manito2();
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_gas}", "click", function(sym, e) {
         sym.$("boton_gas").hide();
         sym.$("boton_gas_hover").show();
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: GAS
         sym.$("icon_gas").show();
         sym.$("icon_gas2").show();
         sym.$("icon_gas3").show();
         sym.$("icon_gas4").show();
         sym.$("icon_gas5").show();
         sym.$("icon_gas6").show();
         sym.$("icon_gas7").show();
         sym.$("icon_gas8").show();
         sym.$("icon_gas9").show();
         sym.$("icon_gas10").show();
         sym.$("icon_gas11").show();
         sym.$("icon_gas12").show();
         sym.$("icon_gas13").show();
         sym.$("icon_gas14").show();
         sym.$("icon_gas15").show();
         sym.$("icon_gas16").show();
         sym.$("icon_gas17").show();
         sym.$("icon_gas18").show();
         /*
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas3").stop("ok");
         sym.getSymbol("icon_gas4").stop("ok");
         sym.getSymbol("icon_gas5").stop("ok");
         sym.getSymbol("icon_gas6").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas8").stop("ok");
         sym.getSymbol("icon_gas9").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas13").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         */
         
         ///manitos
         sym.$("icon_mano3").show();
         sym.$("icon_mano7").show();
         sym.$("icon_mano8").show();
         sym.$("icon_mano10").show();
         sym.$("icon_mano12").show();
         sym.$("icon_mano16").show();
         
         ok1 = 0;
         sym.$("icon_mano4").show();
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas3").stop("ok");
         sym.getSymbol("icon_gas4").stop("ok");
         sym.getSymbol("icon_gas5").stop("ok");
         sym.getSymbol("icon_gas6").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas8").stop("ok");
         sym.getSymbol("icon_gas9").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas13").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas17").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_gas_hover}", "click", function(sym, e) {
         sym.$("boton_gas").show();
         sym.$("boton_gas_hover").hide();
         //sym.$("mensaje").hide();
         //sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: GAS
         sym.$("icon_gas").hide();
         sym.$("icon_gas2").hide();
         sym.$("icon_gas3").hide();
         sym.$("icon_gas4").hide();
         sym.$("icon_gas5").hide();
         sym.$("icon_gas6").hide();
         sym.$("icon_gas7").hide();
         sym.$("icon_gas8").hide();
         sym.$("icon_gas9").hide();
         sym.$("icon_gas10").hide();
         sym.$("icon_gas11").hide();
         sym.$("icon_gas12").hide();
         sym.$("icon_gas13").hide();
         sym.$("icon_gas14").hide();
         sym.$("icon_gas15").hide();
         sym.$("icon_gas16").hide();
         sym.$("icon_gas17").hide();
         sym.$("icon_gas18").hide();
         /*
         sym.getSymbol("icon_gas").stop("inicio");
         sym.getSymbol("icon_gas2").stop("inicio");
         sym.getSymbol("icon_gas3").stop("inicio");
         sym.getSymbol("icon_gas4").stop("inicio");
         sym.getSymbol("icon_gas5").stop("inicio");
         sym.getSymbol("icon_gas6").stop("inicio");
         sym.getSymbol("icon_gas7").stop("inicio");
         sym.getSymbol("icon_gas8").stop("inicio");
         sym.getSymbol("icon_gas9").stop("inicio");
         sym.getSymbol("icon_gas10").stop("inicio");
         sym.getSymbol("icon_gas11").stop("inicio");
         sym.getSymbol("icon_gas12").stop("inicio");
         sym.getSymbol("icon_gas13").stop("inicio");
         sym.getSymbol("icon_gas14").stop("inicio");
         sym.getSymbol("icon_gas15").stop("inicio");
         sym.getSymbol("icon_gas16").stop("inicio");
         */
         //manitos
         sym.$("icon_mano3").hide();
         sym.$("icon_mano7").hide();
         sym.$("icon_mano8").hide();
         sym.$("icon_mano10").hide();
         sym.$("icon_mano12").hide();
         sym.$("icon_mano16").hide();
         
         
         ///
         
         ok1 = 1;
         sym.manito1();
         
         sym.getSymbol("cuadros_popup").stop("inicio");
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         
         
         ///manos
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop1");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").play("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         
         ///manos
         sym.getSymbol("icon_mano").stop("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");
         

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano2}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop2");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").play("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").stop("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano3}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop3");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").play("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").stop("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano4}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop4");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").play("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").play("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").stop("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano5}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop5");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',5);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").play("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").stop("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano6}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop6");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").play("ok");
         sym.getSymbol("icon_petroleo4").play("ok");
         sym.getSymbol("icon_petroleo5").play("ok");
         sym.getSymbol("icon_petroleo6").play("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").stop("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano7}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop7");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").play("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").stop("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano8}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop8");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").play("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").stop("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano9}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop9");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").play("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").stop("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano10}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop10");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").play("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").stop("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano11}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop11");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").play("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").play("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").stop("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano12}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop12");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").play("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         //
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").stop("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano13}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop13");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").play("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").stop("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano14}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop14");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").play("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").stop("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano15}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop15");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").play("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").stop("ok");
         sym.getSymbol("icon_gas15").stop("ok");
         sym.getSymbol("icon_gas16").stop("ok");
         sym.getSymbol("icon_gas18").stop("ok");
         
         ///
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").stop("ok");
         sym.getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano16}", "click", function(sym, e) {
         sym.getSymbol("cuadros_popup").play("pop16");
         sym.$("mensaje").hide();
         sym.buenas = 1;
         //
         sym.$("mano1").css('zIndex',20);
         sym.$("mano2").css('zIndex',20);
         sym.$("mano3").css('zIndex',20);
         sym.$("mano4").css('zIndex',20);
         sym.$("mano5").css('zIndex',20);
         sym.$("mano6").css('zIndex',20);
         sym.$("mano7").css('zIndex',20);
         sym.$("mano8").css('zIndex',20);
         sym.$("mano9").css('zIndex',20);
         sym.$("mano10").css('zIndex',20);
         sym.$("mano11").css('zIndex',20);
         sym.$("mano12").css('zIndex',20);
         sym.$("mano13").css('zIndex',20);
         sym.$("mano14").css('zIndex',20);
         sym.$("mano15").css('zIndex',20);
         sym.$("mano16").css('zIndex',20);
         
         /////////ICONOS: CARBON
         sym.getSymbol("icon_carbon").stop("ok");
         sym.getSymbol("icon_carbon3").stop("ok");
         sym.getSymbol("icon_carbon5").stop("ok");
         sym.getSymbol("icon_carbon7").stop("ok");
         sym.getSymbol("icon_carbon10").stop("ok");
         sym.getSymbol("icon_carbon11").stop("ok");
         sym.getSymbol("icon_carbon12").stop("ok");
         sym.getSymbol("icon_carbon13").stop("ok");
         
         /////////ICONOS: PETROLEO
         sym.getSymbol("icon_petroleo").stop("ok");
         sym.getSymbol("icon_petroleo2").stop("ok");
         sym.getSymbol("icon_petroleo3").stop("ok");
         sym.getSymbol("icon_petroleo4").stop("ok");
         sym.getSymbol("icon_petroleo5").stop("ok");
         sym.getSymbol("icon_petroleo6").stop("ok");
         sym.getSymbol("icon_petroleo7").stop("ok");
         sym.getSymbol("icon_petroleo10").stop("ok");
         sym.getSymbol("icon_petroleo11").stop("ok");
         sym.getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getSymbol("icon_gas").stop("ok");
         sym.getSymbol("icon_gas2").stop("ok");
         sym.getSymbol("icon_gas7").stop("ok");
         sym.getSymbol("icon_gas10").stop("ok");
         sym.getSymbol("icon_gas11").stop("ok");
         sym.getSymbol("icon_gas12").stop("ok");
         sym.getSymbol("icon_gas14").play("ok");
         sym.getSymbol("icon_gas15").play("ok");
         sym.getSymbol("icon_gas16").play("ok");
         sym.getSymbol("icon_gas18").play("ok");
         ////
         sym.getSymbol("icon_mano").play("ok");
         sym.getSymbol("icon_mano2").play("ok");
         sym.getSymbol("icon_mano3").play("ok");
         sym.getSymbol("icon_mano4").play("ok");
         sym.getSymbol("icon_mano5").play("ok");
         sym.getSymbol("icon_mano6").play("ok");
         sym.getSymbol("icon_mano7").play("ok");
         sym.getSymbol("icon_mano8").play("ok");
         sym.getSymbol("icon_mano9").play("ok");
         sym.getSymbol("icon_mano10").play("ok");
         sym.getSymbol("icon_mano11").play("ok");
         sym.getSymbol("icon_mano12").play("ok");
         sym.getSymbol("icon_mano13").play("ok");
         sym.getSymbol("icon_mano14").play("ok");
         sym.getSymbol("icon_mano15").play("ok");
         sym.getSymbol("icon_mano16").stop("ok");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.stop();
         
         sym.getSymbol("icon_mano").play("inicio");
         sym.getSymbol("icon_mano2").play("inicio");
         sym.getSymbol("icon_mano3").play("inicio");
         sym.getSymbol("icon_mano4").play("inicio");
         sym.getSymbol("icon_mano5").play("inicio");
         sym.getSymbol("icon_mano6").play("inicio");
         sym.getSymbol("icon_mano7").play("inicio");
         sym.getSymbol("icon_mano8").play("inicio");
         sym.getSymbol("icon_mano9").play("inicio");
         sym.getSymbol("icon_mano10").play("inicio");
         sym.getSymbol("icon_mano11").play("inicio");
         sym.getSymbol("icon_mano12").play("inicio");
         sym.getSymbol("icon_mano13").play("inicio");
         sym.getSymbol("icon_mano14").play("inicio");
         sym.getSymbol("icon_mano15").play("inicio");
         sym.getSymbol("icon_mano16").play("inicio");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_este_norte}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_este_sur}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_oeste_norte}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_oeste_sur}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano16}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano15}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano14}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano13}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano12}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano11}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano10}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano9}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano8}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano7}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano6}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano5}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano4}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano3}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano2}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${icon_mano}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_gas_hover}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_gas}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_petroleo_hover}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_petroleo}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_carbon_hover}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${boton_carbon}", "touchstart", function(sym, e) {
         $(e.currentTarget).click();

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'icon_carbon'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.play("inicio");

      });
      //Edge binding end

   })("icon_carbon");
   //Edge symbol end:'icon_carbon'

   //=========================================================
   
   //Edge symbol: 'icon_petroleo'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.play("inicio");

      });
      //Edge binding end

   })("icon_petroleo");
   //Edge symbol end:'icon_petroleo'

   //=========================================================
   
   //Edge symbol: 'icon_gas'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.play("inicio");

      });
      //Edge binding end

   })("icon_gas");
   //Edge symbol end:'icon_gas'

   //=========================================================
   
   //Edge symbol: 'icon_mano'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5830, function(sym, e) {
         // introducir código aquí
         // Reproducir la línea de tiempo en un momento o etiqueta específicos. Por ejemplo:
         // sym.play(500); o sym.play("myLabel");
         sym.play("press");

      });
      //Edge binding end

   })("icon_mano");
   //Edge symbol end:'icon_mano'

   //=========================================================
   
   //Edge symbol: 'cuadros_popup'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${parhce_loco}", "click", function(sym, e) {
         sym.stop("inicio");
         sym.getComposition().getStage().$("mensaje").hide();
         sym.getComposition().getStage().buenas = 1;
         //
         sym.getComposition().getStage().$("mano1").css('zIndex',20);
         sym.getComposition().getStage().$("mano2").css('zIndex',20);
         sym.getComposition().getStage().$("mano3").css('zIndex',20);
         sym.getComposition().getStage().$("mano4").css('zIndex',20);
         sym.getComposition().getStage().$("mano5").css('zIndex',20);
         sym.getComposition().getStage().$("mano6").css('zIndex',20);
         sym.getComposition().getStage().$("mano7").css('zIndex',20);
         sym.getComposition().getStage().$("mano8").css('zIndex',20);
         sym.getComposition().getStage().$("mano9").css('zIndex',20);
         sym.getComposition().getStage().$("mano10").css('zIndex',20);
         sym.getComposition().getStage().$("mano11").css('zIndex',20);
         sym.getComposition().getStage().$("mano12").css('zIndex',20);
         sym.getComposition().getStage().$("mano13").css('zIndex',20);
         sym.getComposition().getStage().$("mano14").css('zIndex',20);
         sym.getComposition().getStage().$("mano15").css('zIndex',20);
         sym.getComposition().getStage().$("mano16").css('zIndex',20);
         
         
         /////////ICONOS: CARBON
         sym.getComposition().getStage().getSymbol("icon_carbon").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon3").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon5").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon7").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon10").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon11").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon12").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_carbon13").stop("ok");
         
         
         
         
         
         
         /////////ICONOS: PETROLEO
         sym.getComposition().getStage().getSymbol("icon_petroleo").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo2").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo3").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo4").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo5").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo6").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo7").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo10").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo11").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_petroleo12").stop("ok");
         
         /////////ICONOS: GAS
         sym.getComposition().getStage().getSymbol("icon_gas").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas2").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas7").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas10").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas11").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas12").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas14").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas15").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas16").stop("ok");
         sym.getComposition().getStage().getSymbol("icon_gas18").stop("ok");
         
         ///manos
         sym.getComposition().getStage().getSymbol("icon_mano").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano2").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano3").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano4").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano5").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano6").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano7").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano8").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano9").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano10").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano11").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano12").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano13").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano14").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano15").play("ok");
         sym.getComposition().getStage().getSymbol("icon_mano16").play("ok");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5500, function(sym, e) {
         // introducir código aquí
         sym.stop();
         
         
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 8500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 9500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 10500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 11500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 12500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 13500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 14500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 15500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 16500, function(sym, e) {
         // introducir código aquí
         sym.stop();

      });
      //Edge binding end

   })("cuadros_popup");
   //Edge symbol end:'cuadros_popup'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-7761605");